-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Dic 07, 2020 alle 10:40
-- Versione del server: 5.7.32-0ubuntu0.18.04.1
-- Versione PHP: 7.1.33-24+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

--
-- Dump dei dati per la tabella `node_tree`
--

INSERT INTO `node_tree` (`id_node`, `level`, `i_left`, `i_right`) VALUES
(1, 2, 2, 3),
(2, 2, 4, 5),
(3, 2, 6, 7),
(4, 2, 8, 9),
(5, 1, 1, 24),
(6, 2, 10, 11),
(7, 2, 12, 19),
(8, 3, 15, 16),
(9, 3, 17, 18),
(10, 2, 20, 21),
(11, 3, 13, 14),
(12, 2, 22, 23);

--
-- Dump dei dati per la tabella `node_tree_names`
--

INSERT INTO `node_tree_names` (`id_node`, `language`, `node_name`) VALUES
(1, 'italian', 'Marketing'),
(1, 'english', 'Marketing'),
(2, 'italian', 'Supporto tecnico'),
(2, 'english', 'Help desk'),
(3, 'italian', 'Manager'),
(3, 'english', 'Managers'),
(4, 'italian', 'Assistenza Cliente'),
(4, 'english', 'Customer Account'),
(5, 'italian', '0brand'),
(5, 'english', '0brand'),
(6, 'italian', 'Amministrazione'),
(6, 'english', 'Accounting'),
(7, 'italian', 'Supporto vendite'),
(7, 'english', 'Sales'),
(8, 'italian', 'Italia'),
(8, 'english', 'Italy'),
(9, 'italian', 'Europa'),
(9, 'english', 'Europe'),
(10, 'italian', 'Europa'),
(10, 'english', 'Europe'),
(11, 'italian', 'Nord America'),
(11, 'english', 'North America'),
(12, 'italian', 'Controllo qualità'),
(12, 'english', 'Quality assurance');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
