-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Dic 07, 2020 alle 10:40
-- Versione del server: 5.7.32-0ubuntu0.18.04.1
-- Versione PHP: 7.1.33-24+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `node_tree`
--

CREATE TABLE `node_tree` (
  `id_node` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `i_left` int(11) NOT NULL,
  `i_right` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `node_tree_names`
--

CREATE TABLE `node_tree_names` (
  `id_node` int(11) NOT NULL,
  `language` enum('italian','english') NOT NULL,
  `node_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `node_tree`
--
ALTER TABLE `node_tree`
  ADD PRIMARY KEY (`id_node`);

--
-- Indici per le tabelle `node_tree_names`
--
ALTER TABLE `node_tree_names`
  ADD UNIQUE KEY `id_node` (`id_node`,`language`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `node_tree`
--
ALTER TABLE `node_tree`
  MODIFY `id_node` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `node_tree_names`
--
ALTER TABLE `node_tree_names`
  ADD CONSTRAINT `id_node_node_tree` FOREIGN KEY (`id_node`) REFERENCES `node_tree` (`id_node`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
