<?php

class NodeTree
{
    /** Trova un nodo a partire dall'id
     * @param $id
     * @param $conn connessione al db
     * @return array i campi del db in array o null
     */
    public static function get($id, $conn)
    {
        $query = "SELECT * FROM `node_tree` where id_node = " . $id;
        $parent_result = $conn->query($query);
        return $parent_result->fetch();
    }

    /** Ottiene i discendenti del nodo indicato dall'id in ingresso nella lingua specificata.
     * Permette di filtrare i risultati tramite una parola chiave.
     * @param $id id del nodo padre
     * @param $language lingua dell'etichetta
     * @param $first primo risultato richiesto
     * @param $page_size numero dei risultati per pagina
     * @param $search_keyword parola chiave per il filtro
     * @param $conn connessione al db
     * @return array contenente i risultati
     * @throws Exception se l'id padre non esiste
     */
    public static function getSubTree($id, $language, $first, $page_size, $search_keyword, $conn)
    {
        $parent = self::get($id, $conn);
        if (empty($parent))
            throw new Exception("ID nodo non valido");
        $query = "SELECT DISTINCT Child.id_node , Child.i_left, Child.i_right, names.node_name as name , count(Descendant.id_node) as descendant

FROM node_tree as Child, node_tree as Parent, node_tree as Descendant,

(select * from node_tree_names where language = \"" . $language . "\" ) as names

WHERE Parent.i_left <= Child.i_left

AND Parent.i_right >= Child.i_right

AND Child.id_node = names.id_node

AND Child.i_left <= Descendant.i_left

AND Child.i_right >= Descendant.i_right

AND Parent.i_left = " . $parent["i_left"];
        if (!empty($search_keyword))
            $query .= " AND names.node_name LIKE '%".$search_keyword."%' ";
        $query .= " GROUP BY Child.id_node, Child.i_left, Child.i_right limit " . $first . ", " . $page_size;

        // per i caratteri speciali
        $conn->query("SET NAMES 'utf8';");
        $tree_result = $conn->query($query);

        return $tree_result->fetchAll();
    }

}