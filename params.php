<?php
if (!isset($_REQUEST["node_id"]) || empty($_REQUEST["node_id"])) {
    $json = (new MyResponseKo("Parametri obbligatori mancanti"))->json();
    echo ($json);
    die();
}
$node_id = $_REQUEST["node_id"];

if (!isset($_REQUEST["language"]) && !empty($_REQUEST["language"]) &&
    ($_REQUEST["language"] == "english" || $_REQUEST["italian"]))
{
    $json = (new MyResponseKo("Parametri obbligatori mancanti"))->json();
    echo ($json);
    die();
}
$language = $_REQUEST["language"];

if (isset($_REQUEST["search_keyword"]) && !empty($_REQUEST["search_keyword"]))
    $search_keyword = $_REQUEST["search_keyword"];

if (isset($_REQUEST["page_num"]) && !empty($_REQUEST["page_num"])) {
    if (!ctype_digit($_REQUEST["page_num"]))
    {
        $json = (new MyResponseKo("Numero di pagina richiesto non valido"))->json();
        echo ($json);
        die();
    }
    $page_num = $_REQUEST["page_num"];
}
if (isset($_REQUEST["page_size"]) && !empty($_REQUEST["page_size"])) {
    if ($_REQUEST["page_size"] < 0 || $_REQUEST["page_size"] > 100)
        {
            $json = (new MyResponseKo("Richiesto formato pagina non valido"))->json();
            echo ($json);
            die();
        }
    $page_size = $_REQUEST["page_size"];
}
