<?php

include ("NodeTree.php");
include ("connection.php");
include ("MyResponseOk.php");
include ("MyResponseKo.php");

// PARAMS DEFAULT
$node_id = null; // *
$language = null; // *
$search_keyword = null;
$page_num = 1; // la prima pagina è la 1
$page_size = 100;

include ("params.php");
$first = ($page_num - 1) * $page_size;
try {
    $subtree = NodeTree::getSubTree($node_id, $language, $first, $page_size, $search_keyword, $conn);
    $r = new MyResponseOk($subtree, $page_num);
} catch (Exception $e) {
    $r = new MyResponseKo($e->getMessage());
}
echo($r->json());





