<?php


class MyResponseOk
{
    function __construct($subTree, $page)
    {
        $this->subTree = $subTree;
        $this->page = $page;
    }

    /** Costruisce l'array associativo dei risultati
     * @return array
     */
    public function build()
    {
        $response = [];
        $response["next_page"] = $this->page + 1;
        if ($this->page > 1)
            $response["prev_page"] = $this->page - 1;
        $response["nodes"] = $this->buildNodes();
        return $response;
    }

    private function buildNodes()
    {
        $responseNodes = [];
        foreach ($this->subTree as $node) {
            $responseNode = [];
            $responseNode["node_id"] = $node["id_node"];
            $responseNode["name"] = $node["name"];
            $responseNode["children_count"] = $node["descendant"];
            $responseNodes[] = $responseNode;
        }
        return $responseNodes;
    }

    /** Ritorna i risultati in formato json
     *  @return string il testo contiene i risultati in formato json
     */
    public function json()
    {
        return json_encode($this->build());
    }
}
