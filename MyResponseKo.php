<?php


class MyResponseKo
{
    function __construct($message)
    {
        $this->message = $message;
    }

    function build() {
        $response = [];
        $response["error"] = $this->message;
        return $response;
    }

    public function json()
    {
        return json_encode($this->build());
    }
}
